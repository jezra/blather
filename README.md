# Blather

Blather is a speech recognizer that will run commands when a user speaks specific keywords. 

## Requirements

1. pocketsphinx
2. gstreamer-1.x (and what ever plugin has pocket sphinx support)
3. Python3
4. GI (GObject Introspection)
5.  pyQt (only required for the Qt based UI)


## Usage

0. move commands.tmp to ~/.config/blather/commands.conf
1. commands.conf is a key:value pair file. On each line, enter the keywords you want, followed by a ':' , followed by the command to want to run.
2. run `Blather.py -r` , this will create the keyword file based on the text in the commands.conf file and Blather will start listening
3. start talking

`./Blather -h` will display the command line options

    Usage: Blather.py [options]

    Options:
      -h, --help            show this help message and exit
      -i INTERFACE, --interface=INTERFACE
                            Interface to use (if any). 'q' for Qt, 'g' for GTK,
                            'gt' for GTK system tray icon
      -r, --refresh         refresh the keyword file. Useful after modifying the
                            commands.conf
      -c, --continuous      starts interface with 'continuous' listen enabled
      -p, --pass-words      passes the recognized words as arguments to the shell
                            command
      -o, --override        override config file with command line options
      -H HISTORY, --history=HISTORY
                            number of commands to store in history file
      -m MICROPHONE, --microphone=MICROPHONE
                            Audio input card to use (if other than system default)
      --valid-sentence-command=VALID_SENTENCE_COMMAND
                            command to run when a valid sentence is detected
      --invalid-sentence-command=INVALID_SENTENCE_COMMAND
                            command to run when an invalid sentence is detected






**Note:** to start Blather without needing to enter command line options all the time, copy options.conf.tmp to ~/.config/blather/options.conf and edit accordingly.


### Examples

* To run blather with the GTK UI and start in continuous listen mode:
`./Blather.py -i g -c`

* To run blather with no UI and using a USB microphone recognized and device 2:
`./Blather.py -m 2`

* To have blather pass the matched sentence to the executing command:
 `./Blather.py -p`

 	**explanation:** if the commands.conf contains:
 **good morning world : example_command.sh**
 then 3 arguments, 'good', 'morning', and 'world' would get passed to example_command.sh as
 `example_command.sh good morning world`

* To run a command when a valid sentence has been detected:
	`./Blather.py --valid-sentence-command=/path/to/command`
	**note:** this can be set in the options.conf  file
* To run a command when a invalid sentence has been detected:
	`./Blather.py --invalid-sentence-command=/path/to/command`
	**note:** this can be set in the options.conf file

### Finding the Device Number of a USB microphone
There are a few ways to find the device number of a USB microphone.

* `cat /proc/asound/cards`
* `arecord -l`
	**note:** this can be set in the options.conf file
